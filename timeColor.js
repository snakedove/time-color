if (typeof $ !== 'undefined' && typeof $.fn !== 'undefined') {
    var daySeconds = 24 * 60 * 60,
        daySecondsThird = daySeconds / 3,
        opacity = 0,
        blue = 0,
        red = 0,
        green = 0;

    $.fn.timeColor = function (options) {
        var $el = $(this),
            showSeconds = !options || typeof options.showSeconds === 'undefined' ? true : options.showSeconds,
            intervalSecs = showSeconds ? 2000 : 60000;

        function getColor() {
            var date = new Date(),
                seconds = date.getSeconds(),
                minutes = date.getMinutes(),
                hours = date.getHours(),
                minuteDegrees = 10 / 60,
                timeNow = (hours * 60 * 60) + (minutes * 60) + seconds,
                degree = 255 / daySecondsThird;

            opacity = Math.round(minuteDegrees * minutes) / 10;

            if (opacity > 0.9) {
                opacity = 0.9;
            }
            if (opacity < 0.4) {
                opacity = 0.4;
            }

            if(timeNow > daySecondsThird * 2) {
                blue = Math.round((timeNow - (daySecondsThird * 2)) * degree);
                red = 255 - Math.round((timeNow - (daySecondsThird * 2)) * degree);
            } else if(timeNow > daySecondsThird && timeNow < daySecondsThird * 2) {
                red = Math.round((timeNow - (daySecondsThird * 2)) * degree);
                green = 255 - Math.round((timeNow - (daySecondsThird * 2)) * degree);
            } else if(timeNow > 0 && timeNow < daySecondsThird) {
                green = Math.round((timeNow - (daySecondsThird * 2)) * degree);
                blue = 255 - Math.round((timeNow - (daySecondsThird * 2)) * degree);
            }

            $el.css({
                'background-color': 'rgb(' + red + ', ' + green + ', ' + blue + ')',
                'opacity': opacity
            });

            if (showSeconds) {
                if (seconds % 60 === 0) {
                    $el.fadeTo(1000, opacity - 0.1, function(){
                        $el.fadeTo(1000, opacity + 0.1);
                    });
                } else {
                    $el.fadeTo(1000, opacity - 0.1, function(){
                        $el.fadeTo(1000, opacity);
                    });
                }
            }
        }

        $(document).ready(function() {
            getColor();
            setInterval(function() {
                getColor();
            }, intervalSecs);
        });
    };
}